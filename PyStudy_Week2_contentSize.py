#需求：使用文件和目录操作，定义一个统计指定目录大小的函数（注意目录中还有子目录）
import os

def content_size(content):
    '''
    定义统计指定目录大小的函数
    '''

    size = 0
    #判断为文件
    if os.path.isfile(content):
        size += os.path.getsize(content)

    #判断为文件夹
    if os.path.isdir(content):
        dir_list = os.listdir(content)
        for i in dir_list:
            content_new = os.path.join(content,i)
            #判断为文件，则进行累加
            if os.path.isfile(content_new):
                size += os.path.getsize(content_new)
            #判断为子目录，则进行递归调用
            if os.path.isdir(content_new):
                size += content_size(content_new)

    return size

#测试
content = input("请输入指定的文件目录：")
print("该目录的大小为：%d KB"%content_size(content))