#导入相应的模块
'''
完成本周1.15的阶段案例《飞机大战》游戏中没有完成的部分。
    1.完成敌机发射子弹功能（注意：子弹不是连发、移动速度不要太快）    √
    2.实现敌机子弹和玩家飞机的碰撞检测
    3.为消失的飞机添加爆炸效果     √
'''
'''第二个 玩家飞机的碰撞检测没写出来 HeroPlane类的display里面写了一些代码 但是执行之后程序会卡死 后来改过依然不行'''

import pygame
from pygame.locals import *
import time,random

#定义一个飞机类
class HeroPlane:
    '''
    玩家飞机(英雄)
    '''
    def __init__(self,screen_temp):
        self.x = 200
        self.y = 400
        self.screen = screen_temp
        self.image = pygame.image.load("./images/me.png")
        self.bullet_list = []   #用于存放玩家的子弹列表
        #爆炸属性
        self.hit = False        #表示是否要爆炸
        self.bomb_list = []     #用来存储爆炸时需要的图片


    def bomb(self):
        self.hit = True

    def display(self):
        #绘制子弹
        for b in self.bullet_list:
            b.display()
            if b.move():
                self.bullet_list.remove(b)

        '''敌机子弹的碰撞检测---没写出来'''
        # if bo.x > self.x+12 and bo.x < self.x+92 and bo.y > self.y+20 and bo.y < self.y+60:
        #     em.bullet_list.remove(bo)
        #     for i in range(1,5):
        #         self.bomb_list.append(pygame.image.load("./images/boom" + str(i) + ".png"))
        #         self.screen.blit(self.bomb_list[i-1], (self.x, self.y))
        #         self.bomb_list[i-1]=0
        #         self.screen.blit(pygame.image.load("./images/gameover.png"), (122, 213))
        #         time.sleep(1)
        #         self.screen.blit(pygame.image.load("./images/continue.png"), (0, 0))
        #         input()


        #绘制玩家飞机
        self.screen.blit(self.image,(self.x,self.y))

    def move_left(self):
        '''左移动飞机'''
        self.x -= 5
        if self.x <= 0:
            self.x = 0

    def move_up(self):
        '''上移动飞机'''
        self.y -= 5
        if self.y <= 0:
            self.y = 0

    def move_right(self):
        '''右移动飞机'''
        self.x += 5
        if self.x >= 406:
            self.x = 406

    def move_down(self):
        '''左移动飞机'''
        self.y += 5
        if self.y >= 492:
            self.y = 492

    def fire(self):
        self.bullet_list.append(Bullet(self.screen,self.x,self.y))
        print(len(self.bullet_list))





#定义一个子弹类
class Bullet:
    '''子弹类'''
    def __init__(self,screen_temp,x,y):
        self.x = x+53
        self.y = y
        self.screen = screen_temp
        self.image = pygame.image.load("./images/pd.png")

    def display(self):
        '''绘制子弹'''
        self.screen.blit(self.image,(self.x,self.y))

    def move(self):
        self.y -= 10
        if self.y <= -20:
            return True

class EnemyPlane:
    '''敌机类'''
    def __init__(self,screen_temp):
        self.x = random.choice(range(410))
        self.y = -80
        self.screen = screen_temp
        self.image = pygame.image.load("./images/e"+str(random.choice(range(3)))+".png")
        self.bullet_list = []
        #爆炸属性
        self.hit = False        #表示是否要爆炸
        self.bomb_list = []     #用来存储爆炸时需要的图片

    def bomb(self):
        self.hit = True

    def display(self):
        '''
        绘制敌机子弹
        '''
        for b in self.bullet_list:
            b.display()
            if b.move():
                self.bullet_list.remove(b)
        if self.hit == True:
            '''绘制爆炸'''
            for i in range(1,5):
                self.bomb_list.append(pygame.image.load("./images/boom" + str(i) + ".png"))
                self.screen.blit(self.bomb_list[i-1], (self.x, self.y))
                self.bomb_list[i-1]=0


        else:
            '''绘制敌机'''
            self.screen.blit(self.image, (self.x, self.y))

    def move(self,hero):
        self.y += 4
        #敌机出屏幕
        if self.y > 568:
            return True

        #遍历所有子弹，并执行碰撞检测
        for bo in hero.bullet_list:
            if bo.x > self.x+12 and bo.x < self.x+92 and bo.y > self.y+20 and bo.y < self.y+60:
                hero.bullet_list.remove(bo)
                self.bomb()     #判断爆炸，更改碰撞索引
                self.display()      #根据碰撞索引，输出爆炸图片
                return True
        #if self.y <= -20:
        #    return True

    def fire(self,index):
        '''添加敌机子弹'''
        if index % 40 == 0:
            self.bullet_list.append(Enemy_Bullet(self.screen, self.x, self.y))
            print(len(self.bullet_list))

class Enemy_Bullet:
    '''子弹类'''
    def __init__(self,screen_temp,x,y):
        self.x = x+52
        self.y = y+20
        self.screen = screen_temp
        self.image = pygame.image.load("./images/enemy_bullet.png")

    def display(self):
        '''绘制子弹'''
        self.screen.blit(self.image,(self.x,self.y))


    def move(self):
        self.y += 18
        if self.y >= 568:
            return True


def key_control(hero_temp):
    '''键盘控制函数'''
    #执行退出操作
    for event in pygame.event.get():
        if event.type == QUIT:
            print("exit()")
            exit()

    #获取按键信息
    pressed_keys = pygame.key.get_pressed()
    #print(pressed_keys)

    #做判断，并执行对象的操作
    if pressed_keys[K_LEFT] or pressed_keys[K_a]:
        print("Left...")
        hero_temp.move_left()
    elif pressed_keys[K_RIGHT] or pressed_keys[K_d]:
        print("Right...")
        hero_temp.move_right()
    elif pressed_keys[K_UP] or pressed_keys[K_w]:
        print("Up...")
        hero_temp.move_up()
    elif pressed_keys[K_DOWN] or pressed_keys[K_s]:
        print("Down...")
        hero_temp.move_down()

    if pressed_keys[K_SPACE]:
        print("Space...")
        hero_temp.fire()



def main():
    '''主程序函数'''
    #创建游戏窗口
    screen = pygame.display.set_mode((512,568),0,0)

    #创建一个游戏背景
    background = pygame.image.load("./images/bg2.jpg")

    #创建玩家飞机
    hero = HeroPlane(screen)

    m = -968
    enemylist = []  #存放敌机的列表
    index = 1
    while True:
        #绘制画面
        screen.blit(background,(0,m))
        m+=2
        if m>=-200:
            m = -968

        #绘制玩家飞机
        hero.display()

        #执行键盘控制
        key_control(hero)

        #随机绘制敌机
        if random.choice(range(50)) == 10:
            enemylist.append(EnemyPlane(screen))

        if index > 80:
            index = 1
        #遍历敌机并绘制移动
        for em in enemylist:
            em.display()
            #if index % 30 == 0:
            em.fire(index)
            if em.move(hero):

                enemylist.remove(em)



        index += 1
        #更新显示
        pygame.display.update()

        #定时显示
        time.sleep(0.04)

#判断当前是否是主运行程序，并调用
if __name__ == "__main__":
    main()
